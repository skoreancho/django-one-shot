from django.forms import ModelForm
from todos.models import TodoList


class NewListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class EditListNameForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
