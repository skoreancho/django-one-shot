from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import NewListForm, EditListNameForm


def todo_list(request):
    todolists = TodoList.objects.all()
    context = {"todolists": todolists}
    return render(request, "todo_list/list.html", context)


# Create your views here.
def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {"todolist": todolist}
    print(todolist.name)
    return render(request, "todo_list/list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = NewListForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = NewListForm()
    context = {
        "form": form,
    }

    return render(request, "todo_list/create.html", context)


def todo_list_update(request, id):
    list_item = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = EditListNameForm(request.POST, instance=list_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = EditListNameForm(instance=list_item)

    context = {"form": form}
    return render(request, "todo_list/edit.html", context)


def todo_list_delete(request, id):
    delete_item = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete_item.delete()
        return redirect("todo_list")
    context = {"delete": delete_item}
    return render(request, "todo_list/delete.html", context)
